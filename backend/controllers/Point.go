package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"ovh.feuillatre-extranet/api/models"
)

//GET /departs/:id/points
func FindPointByDepart(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var points []models.Point
	if err := db.Where("depart_id = ?", c.Param("id")).Find(&points).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Records not found!"})
		return
	}
	for i := range points {
		switch points[i].Urgence {
		default:
			points[i].Urgence = "U2"
			break
		case "U1 - 0,60/2m":
		case "U1":
			points[i].Urgence = "U1"
			break
		case "U0 - 0/60Cm":
		case "U0":
			points[i].Urgence = "U0"
			break
		}

	}

	c.JSON(http.StatusOK, gin.H{"points": points})
}

// PATCH /points/:id
func UpdatePoint(c *gin.Context) {

	var input models.UpdatePointInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	fmt.Println(&input)

}
