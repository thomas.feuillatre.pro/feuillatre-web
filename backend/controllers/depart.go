package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"net/http"
	"ovh.feuillatre-extranet/api/models"
)

//GET /departs/:id
func FindDepart(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)
	var depart models.Depart
	if err := db.Where("id = ?", c.Param("id")).First(&depart).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	//var points []models.Point
	if err := db.Where("depart_id = ?", c.Param("id")).Find(&depart.Points).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"depart": depart})

}

//GET /departs
func FindDeparts(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var departs []models.Depart
	db.Find(&departs)

	c.JSON(200, gin.H{"data": departs})
}

//GET /departs/departement/status
func FindDepartsByStatus(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var departs []models.Depart
	if err := db.Where("dep = ? AND status = ?", c.Param("dep"), c.Param("status")).Find(&departs).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Records not found!"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"departs": departs})
}

// POST /departs
func CreateDepart(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Validate input
	var input models.CreateDepartInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Create book
	depart := models.Depart{
		Gdo:    input.Gdo,
		Dep:    input.Dep,
		Status: 0,
	}
	db.Create(&depart)
	c.JSON(http.StatusOK, gin.H{"data": depart})
}

// PATCH /departs/:id
func UpdateDepart(c *gin.Context) {

	db := c.MustGet("db").(*gorm.DB)
	var depart models.Depart
	if err := db.Where("id = ?", c.Param("id")).First(&depart).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}
	var input models.UpdateDepartInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	db.Model(&depart).Update(input)
	c.JSON(http.StatusOK, gin.H{"data": depart})
}

// DELETE /departs/:id
// Delete a Depart
func DeleteDepart(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Get model if exist
	var depart models.Depart
	if err := db.Where("id = ?", c.Param("id")).First(&depart).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&depart)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
