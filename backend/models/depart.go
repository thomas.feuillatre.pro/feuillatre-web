package models

import "encoding/json"

type Depart struct {
	ID           uint            `json:"id" gorm:"primary_key"`
	Name         string          `json:"name"`
	Gdo          string          `json:"gdo"`
	Dep          string          `json:"dep"`
	Status       uint            `json:"status"`
	InseeCommune json.RawMessage `json:"insee_commune" gorm:"type:json"`
	Points       []Point         `json:"points" gorm:"-"`
}

type CreateDepartInput struct {
	Gdo string `json:"gdo" binding:"required"`
	Dep string `json:"dep" binding:"required"`
}

type UpdateDepartInput struct {
	Gdo    string `json:"gdo"`
	Dep    string `json:"dep"`
	Status uint   `json:"status"`
}

//func (Depart) TableName() string {
//	return "depart"
//}
