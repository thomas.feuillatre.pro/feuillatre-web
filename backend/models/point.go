package models

import "time"

type Point struct {
	ID           uint   `json:"id" gorm:"primary_key"`
	Depart       Depart `gorm:"association_foreignkey:ID"`
	DepartID     uint   `json:"depart_id"`
	Urgence      string `json:"urgence"`
	Coupure      uint16 `json:"coupure"`
	DistanceToDo uint16 `json:"distance_to_do"`
	DistanceDone uint16 `json:"distance_done"`
	Abattage     uint8  `json:"abattage"`
	Access       uint8  `json:"access"`
	Surlargeur   uint16 `json:"surlargeur"`
	Observation  string `json:"observation"`
	StatusWork   uint8  `json:"status_work"`
	Essence      string `json:"essence"`
	DistanceSC   uint16 `json:"distance_sc"`

	Timestamp   int64   `json:"timestamp"`
	Responsable string  `json:"responsable"`
	Fic         uint16  `json:"fic"`
	Accord      uint8   `json:"accord"`
	Latitude    float64 `json:"latitude"`
	Longitude   float64 `json:"longitude"`
	TypeWork    string  `json:"type_work"`
}
type UpdatePointInput struct {
	DistanceDone uint16    `json:"distance_done"`
	Abattage     uint8     `json:"abattage"`
	Access       uint8     `json:"access"`
	Surlargeur   uint16    `json:"surlargeur"`
	Observation  string    `json:"observation"`
	StatusWork   uint8     `json:"status_work"`
	Essence      string    `json:"essence"`
	Timestamp    time.Time `json:"timestamp"`
	Responsable  string    `json:"responsable"`
	TypeWork     string    `json:"type_work"`
}

//
//func (Point) TableName() string {
//	return "point"
//}
