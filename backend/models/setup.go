package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func SetupModels() *gorm.DB {
	db, err := gorm.Open("mysql", "jktwyn67fpgepq03:tl4r7vkv8dab2c5y@(nr84dudlpkazpylz.chr7pe7iynqr.eu-west-1.rds.amazonaws.com)/d1dfyletc4hqpo39?charset=utf8&parseTime=True&loc=Local")

	if err != nil {
		panic("Failed to connect to db")
	}
	db.SingularTable(true)
	db.AutoMigrate(&Depart{})
	db.AutoMigrate(&Point{})
	return db
}
