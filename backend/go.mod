module ovh.feuillatre-extranet/api

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.12
	github.com/kr/pretty v0.1.0 // indirect
	golang.org/x/sys v0.0.0-20200501145240-bc7a7d42d5c3 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect

)
