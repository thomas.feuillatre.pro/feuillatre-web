package main

import (
	"github.com/gin-gonic/gin"
	"os"
	"ovh.feuillatre-extranet/api/controllers"
	"ovh.feuillatre-extranet/api/models"
)

func main() {
	os.Setenv("PORT", "5000")
	r := gin.Default()

	db := models.SetupModels()
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
		c.Next()
	})
	r.Use(CORSMiddleware())

	// DEPARTS
	r.GET("/departs/:id", controllers.FindDepart)
	r.GET("/departs", controllers.FindDeparts)
	r.GET("/departsFilter/:dep/:status", controllers.FindDepartsByStatus)
	r.POST("/departs", controllers.CreateDepart)
	r.PATCH("/departs/:id", controllers.UpdateDepart)
	r.DELETE("/departs/:id", controllers.DeleteDepart)

	// POINTS
	r.GET("/departs/:id/points", controllers.FindPointByDepart)
	r.PATCH("/points/:id", controllers.UpdatePoint)
	r.Run()
}
func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With,access-control-allow-methods,access-control-allow-origin")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
