import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'


Vue.use(VueRouter);


const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/home',
        redirect: Home
    },
    {
        path: '/client',
        name: 'client',
        component: () => import('../views/ClientSearch'),
    },
    {
        name: 'DetailClient',
        path: '/client/detail/:id?',
        component: () => import('../views/ClientDetail'),
        props: true
    },
    {
        name: 'DetailDevis',
        path: '/client/devis/:id?',
        props: true,
        component: () => import('../views/DevisDetail')
    },
    {
        path: '/moyens',
        component: () => import('../views/MoyensManager'),
    },
    {
        path: '/vehicles',
        component: () => import('../views/VehicleManager'),
    },
    {
        path: '/departs',
        component: () => import('../views/DepartsDisplay')
    },
];

const router = new VueRouter({
    routes,
    mode: 'history'
});

export default router
