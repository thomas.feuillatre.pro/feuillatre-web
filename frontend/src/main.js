import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import axios from 'axios'
import VuejsDialog from 'vuejs-dialog';
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
import './assets/css/main.css'
import store from './store'

// axios.defaults.baseURL = 'https://dev.feuillatre-extranet.ovh/api/';
axios.defaults.baseURL = 'http://localhost:5000';
axios.defaults.headers.common = {
  ...axios.defaults.headers.common,
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods':'GET, PUT, POST, DELETE, OPTIONS'
}

Vue.config.productionTip = false;
Vue.use(VuejsDialog);


new Vue({
    router,
    vuetify,
    store,
    render: h => h(App)
}).$mount('#app');
