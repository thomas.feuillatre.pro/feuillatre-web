import axios from 'axios'
export default axios.create({
        // baseURL: 'https://dev.feuillatre-extranet.ovh/api/',
        baseURL: 'http://localhost:5000',
        timeout: 3000,
    })