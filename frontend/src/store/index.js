import Vue from 'vue'
import Vuex from 'vuex'
import axios from "../plugins/axios";
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        client: {
            id: null,
            name: null,
            address: null,
            city: null,
            postalCode: null,
            phone: null,
            email: null,
            devis: [],
            phone2: null,
            title: null
        },
        moyens: {}
    },
    mutations: {
        selectClient(state, id) {
            state.client.id = id;
        },
        initClient(state) {
            state.client = {
                id: null,
                name: null,
                address: null,
                city: null,
                postalCode: null,
                phone: null,
                email: null,
                devis: [],
                phone2: null,
                title: null
            }
        },
        updateCient(state, client) {
            state.client = client;
        },
        updateDevis(state, devis) {
            state.client.devis = devis;
        }
    },
    actions: {
        getClient({commit}) {
            if (this.state.client.id) {
                axios.get('getClient/' + this.state.client.id).then((response) => commit('updateCient', response.data))
            } else {
                commit('initClient');
            }
        },
        fetchDevis({commit}, id) {
            axios.get('getDevis/' + id).then((res) => commit('updateDevis', res.data));
        }
    },
    modules: {}
})